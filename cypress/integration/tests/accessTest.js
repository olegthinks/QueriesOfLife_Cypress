/// <reference types="cypress" />

context('Page Check', function () {
  it('Zillow', () => {
        cy.visit('https://www.zillow.com/homedetails/3109-Golden-Valley-Rd-Golden-Valley-MN-55422/1850402_zpid/')
        cy.title().should('contains', '3109')
    })
    it('Redfin', () => {
        cy.visit('https://www.redfin.com/MN/Minneapolis/3109-Golden-Valley-Rd-55422/home/50152075')
        cy.title().should('contains', '3109')
    })
    it('Realtor', () => {
        cy.visit('https://www.realtor.com/realestateandhomes-detail/3109-Golden-Valley-Rd_Golden-Valley_MN_55422_M71725-02341', { force: true,   failOnStatusCode: false})
    })
    it('Trulia', () => {
        cy.visit('https://www.trulia.com/p/mn/golden-valley/3109-golden-valley-rd-golden-valley-mn-55422--2055714019')
        cy.title().should('contains', '3109')
    })
    it('ColdwellBanker', () => {
        cy.visit('https://www.coldwellbankerhomes.com/mn/golden-valley/3109-golden-valley-rd/pid_40440767/')
        cy.title().should('contains', '3109')
    })
})
